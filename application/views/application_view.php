<div>
	<h1><?php echo $job_title->title; ?></h1>

	<?php echo validation_errors(); ?>

	<?php echo form_open('jobs/submit/' . $job_id); ?>

	<p><label for="name">Name</label><input type='text' name="name" value='<?php echo set_value('name');?>' placeholder='name' required/></p>
	<p><label for"email">Email</label><input type="email" name="email" value='<?php echo set_value('email');?>' placeholder='youraddress@mail.com' required/></p>
	<p><label for"phone">Phone</label><input type="tel" name="phone" value='<?php echo set_value('phone');?>' placeholder='123-456-7890' required/></p>
	<p><label for"major">Major</label><input type="text" value='<?php echo set_value('major');?>' name="major" placeholder='' required/></p>
	<p><label for"gpa">GPA</label><input type="text" name="gpa" value='<?php echo set_value('gpa');?>' placeholder='12.0' required/></p>
	<p><label for="abroad">Are you planning on going abroad while at CMC?</label>

	<?php $options = array
	('--' => '--',
	 'spring14' => 'Spring 2014',
	 'fall14' => 'Fall 2014', 
	 'spring15' => "Spring 2015",
	 'no abroad' => 'I do not plan on going abroad');
	echo form_dropdown('abroad', $options); ?>

	<?php foreach($questions as $question) : ?>

		<h2><?php echo $question->question;?><h2>
		<textarea name="question<?php echo $question->id;?>" cols="30" rows="10"><?php echo set_value("question".$question->id);?></textarea>

	<?php endforeach; ?>

	<h2>How did you hear about SOURCE?</h2>

	<p><input type="radio" name="heard" value='Word of Mouth' <?php echo set_radio('heard', 'Word of Mouth'); ?>>Word of Mouth<br></p>
	<p><input type="radio" name="heard" value= 'Open House' <?php echo set_radio('heard', 'Open House'); ?>>Open House<br></p>
	<p><input type="radio" name="heard" value= 'Info Session'<?php echo set_radio('heard', 'Infor Session') ?>>Info Session<br></p>
	<p><input type="radio" name="heard" value= 'Manager Office Hours' <?php echo set_radio('heard', 'Manager Office Hours'); ?>>Manager Office Hours<br></p>
	<p><input type="radio" name="heard" value= 'Fliers' <?php echo set_radio('heard', 'Fliers'); ?>>Fliers<br></p>
	<p><input type="radio" name="heard" value= 'Website' <?php echo set_radio('heard', 'Website'); ?>>Website<br></p>
	<p><input type="radio" name="heard" value= 'Collins Display' <?php echo set_radio('heard', 'Collins Display'); ?>>Collins Display<br></p>
	<p><input type="radio" name="heard" value= 'Other' <?php echo set_radio('heard', 'Other') ?>>Other</p>
	<p><input type='text' name="other" value='<?php echo set_value('other'); ?>'/><p>

	<p> if you heard about source from a current Consultant (or multiple), please list his or her names(s).</p>
	<textarea name="consultant" id="" cols="30" rows="10"><?php echo set_value('consultant'); ?></textarea>

	<?php if($job_id == 1) : ?>

		<h4>What types of work are you mosted interested in? Check all that apply.</h4>

		<p><input type="checkbox" name="work[]" value="Grant Writing" <?php echo set_checkbox('work[]', 'Grant Writing');?>>Grant Writing</p>
		<p><input type="checkbox" name="work[]" value="Grant Research" <?php echo set_checkbox('work[]', 'Grant Reasearch');?>>Grant Research</p>
		<p><input type="checkbox" name="work[]" value="Impact Analysis" <?php echo set_checkbox('work[]', 'Impact Analysis');?>>Impact Analysis</p>
		<p><input type="checkbox" name="work[]" value="Database Collection" <?php echo set_checkbox('work[]', 'Database Collection');?>>Database Collection</p>
		<p><input type="checkbox" name="work[]" value="Evaluations" <?php echo set_checkbox('work[]', 'Evaluations');?>>Evaluations</p>
		<p><input type="checkbox" name="work[]" value="Marketing" <?php echo set_checkbox('work[]', 'Marketing');?>>Marketing</p>
		<p><input type="checkbox" name="work[]" value="Board Development" <?php echo set_checkbox('work[]', 'Board Development');?>>Board Development</p>
		<p><input type="checkbox" name="work[]" value="Fundraising" <?php echo set_checkbox('work[]', 'Fundraising');?>>Fundraising</p>
		
	<?php endif; ?>

	<?php if($job_id == 1 || $job_id == 2) : ?>

		<?php if($job_id == 2) : ?>
			<p>Please have a professor of your choosing send an academic reference</p>			
			<br>
		<?php endif; ?>

		<h4>Peer Reference</h4>
		<p><label for="peer_name">Name:</label><input type="text" name ='peer_name' value='<?php echo set_value('peer_name');?>'/></p>
		<p><label for="peer_affil">Affiliation:</label><input type="text" name ='peer_affil' value='<?php echo set_value('peer_affil');?>'/></p>
		<p><label for="peer_email">Email:</label><input type="email" name ='peer_email' value='<?php echo set_value('peer_email');?>'/></p>
		<p><label for="peer_phone">Phone #:</label><input type="tel" name ='peer_phone' value='<?php echo set_value('peer_phone');?>'/></p>

	<?php endif; ?>

	<?php if($job_id == 1 || $job_id == 3): ?>
		
		<h4>Academic Reference</h4>
		<p><label for="academic_name">Name:</label><input type="text" name ='academic_name' value='<?php echo set_value('academic_name');?>'/></p>
		<p><label for="academic_affil">Affiliation:</label><input type="text" name ='academic_affil' value='<?php echo set_value('academic_affil');?>'/></p>
		<p><label for="academic_email">Email:</label><input type="email" name ='academic_email' value='<?php echo set_value('academic_email');?>'/></p>
		<p><label for="academic_phone">Phone #:</label><input type="tel" name ='academic_phone' value='<?php echo set_value('academic_phone');?>'/></p>

	<?php endif; ?>
	
	<h4>Non-Academic Reference</h4>
	<p><label for="non_name">Name:</label><input type="text" name ='non_name' value='<?php echo set_value('non_name');?>'/></p>
	<p><label for="non_affil">Affiliation:</label><input type="text" name ='non_affil' value='<?php echo set_value('non_affil');?>'/></p>
	<p><label for="non_email">Email:</label><input type="email" name ='non_email' value='<?php echo set_value('non_email');?>'/></p>
	<p><label for="non_phone">Phone #:</label><input type="tel" name ='non_phone' value='<?php echo set_value('non_phone');?>'/></p>

	<?php if($job_id == 2): ?>
		<p><input type="checkbox" name="consultant_check" value="1" <?php echo set_checkbox('consultant_check', '1');?>>If not offered position of lead
			consultant, I would like to be considered for the position of Consultant</p>
	<?php endif; ?>

	<div>
		<input type="submit" name="submit" value="Submit Application">
	</div>	

	</form>
</div>