<div>
		<h1>Available Positions</h1>

		<?php foreach($jobs as $job) : ?>

			<h2><?php echo $job->title; ?></h2>
			<p><?php echo $job->description ?></p>
			<?php echo anchor("jobs/application/" . $job->id, "Apply for this job");?>

		<?php endforeach; ?>
</div>