<?php 

class jobs_model extends CI_Model{

	function getAll(){
		$query = $this->db->get('job_listings');
		if($query->num_rows() > 0){
			return $query->result();
		}
	}

	function getById($id){
		$this->db->where('id', $id);
		$query = $this->db->get('job_listings');
		if($query->num_rows() > 0){
			return $query->row();
		}
	}

	function getQuestionByJob($job_id){
		$this->db->where('job_id', $job_id);
		$query = $this->db->get('questions');
		if($query->num_rows() > 0){
			return $query->result();
		}
	}

	function set_application(){
		$data = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'major' => $this->input->post('major'),
				'gpa' => $this->input->post('gpa'),
				'abroad' => $this->input->post('abroad'),
				'heard' => $this->input->post('heard'),
				'consultant' => $this->input->post('consultant'),
				'work' => serialize($this->input->post('work')),
				'consultant_check' => $this->input->post('consultant_check')
			);
		return $this->db->insert('Applicants', $data);
	}

	function set_questions($questions){
		$data = array();
		foreach ($questions as $question) {
			$new_data = array(
				'question'.$question->id => 
				$this->input->post('question'.$question->id)
			);
			$data = array_merge($data, $new_data);
		}
		return $this->db->insert('Answers', $data);
	}

	function set_references($job_id){
		$data = array();
		if($job_id == 1 || $job_id == 2){
			$array1 = array(
					'peer_name' => $this->input->post('peer_name'),
					'peer_affil' => $this->input->post('peer_affil'),
					'peer_email' => $this->input->post('peer_email'),
					'peer_phone' => $this->input->post('peer_phone')

			);
			$data = array_merge($data, $array1);
		}
		if($job_id == 1 || $job_id == 3){
			$array2 = array(
					'academic_name' => $this->input->post('academic_name'),
					'academic_affil' => $this->input->post('academic_affil'),
					'academic_email' => $this->input->post('academic_email'),
					'academic_phone' => $this->input->post('academic_phone')
			);
			$data = array_merge($data, $array2);
		}
		
		$array3 = array(
				'non_name' => $this->input->post('non_name'),
				'non_affil' => $this->input->post('non_affil'),
				'non_email' => $this->input->post('non_email'),
				'non_phone' => $this->input->post('non_phone')
		);
		$data = array_merge($data, $array3);

		return $this->db->insert('References', $data);
	}
}

