<?php 

class jobs extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('jobs_model');
	}

	function index(){
		$data['jobs'] = $this->jobs_model->getAll();
		$data['main_content'] = 'jobs_view';
		$this->load->view('includes/template', $data);
	}

	function application(){
		$job_id = $this->uri->segment(3);
		$data['job_id'] = $job_id;
		$data['job_title'] = $this->jobs_model->getById($job_id);
		$data['questions'] = $this->jobs_model->getQuestionByJob($job_id);
		$data['main_content'] = 'application_view';
		$this->load->view('includes/template', $data);
	}

	function submit(){
		$job_id = $this->uri->segment(3);
		$questions = $this->jobs_model->getQuestionByJob($job_id);

		if($this->validate($questions, $job_id) == FALSE)
		{
			$this->application();
		}
		else
		{
			$this->jobs_model->set_application();
			$this->jobs_model->set_questions($questions);
			$this->jobs_model->set_references($job_id);
			$data['main_content'] = 'application_submit_view';
			$this->load->view('includes/template', $data);
		}
	}

		function validate($questions, $job_id){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Name', 'trim|required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
		$this->form_validation->set_rules('major', 'Major', 'trim|required|alpha');
		$this->form_validation->set_rules('gpa', 'GPA', 'trim|required');

		foreach ($questions as $question){
			$this->form_validation->set_rules('question'.$question->id, "Question Number" .$question->id, 'trim');
		}

		$this->form_validation->set_rules('heard', 'Types of Work', 'trim');
		$this->form_validation->set_rules('other', 'Other', 'trim');
		$this->form_validation->set_rules('consultant', 'Referring Consultants', 'trim');
		$this->form_validation->set_rules('work[]', 'work', 'trim');

		if($job_id == 1 || $job_id == 2){
			$this->form_validation->set_rules('peer_name', 'Peer Reference Name', 'trim|required|alpha');
			$this->form_validation->set_rules('peer_affil', 'Peer Reference Affiliation', 'trim|required|alpha');
			$this->form_validation->set_rules('peer_email', 'Peer Reference Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('peer_phone', 'Peer Reference phone', 'trim|required');

		}
		if($job_id == 1 || $job_id == 3){
			$this->form_validation->set_rules('academic_name', 'Academic Reference Name', 'trim|required|alpha');
			$this->form_validation->set_rules('academic_affil', 'Academic Reference Affiliation', 'trim|required|alpha');
			$this->form_validation->set_rules('academic_email', 'Academic Reference Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('academic_phone', 'Academic Reference phone', 'trim|required');
		}

		$this->form_validation->set_rules('non_name', 'Non Academic Reference Name', 'trim|required|alpha');
		$this->form_validation->set_rules('non_affil', 'Non Academic Reference Affiliation', 'trim|required|alpha');
		$this->form_validation->set_rules('non_email', 'Non Academic Reference Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('non_phone', 'Non Academic Reference phone', 'trim|required');
		$this->form_validation->set_rules('consultant_check' , 'consultant', 'trim');

		return $this->form_validation->run();
	}
} 