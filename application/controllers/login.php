<?php

class login extends CI_controller{

	function index(){
		$data['main_content'] = "login_view";
		$this->load->view('includes/template' , $data);
	}

	function submit(){
		$this->load->library('form_validation');

		

		if($this->form_validation->run() == FALSE){
			$this->index();
		}else{
			$data['main_content'] = 'member_view';
			$this->load->view('includes/template', $data);
		}
	}
}